from django.shortcuts import render
from basket.forms import DetailsForm
from django.shortcuts import redirect
from django.urls import reverse

import requests

def seat_pick(request):
    if request.method == "POST":
        # Get the posted form
        DetailForm = DetailsForm(request.POST)

        if DetailForm.is_valid():
            username = request.session['temp_data']['name']
            adults = DetailForm.cleaned_data['adults']
            infants = DetailForm.cleaned_data['infants']
            children = DetailForm.cleaned_data['children']
    username = request.session['temp_data']['name']
    adults = request.session['temp_data']['adults']
    infants = request.session['temp_data']['infants']
    children = request.session['temp_data']['children']
    
    #Sending Cache request to Gordian API
    url = "https://test-ancil-overlay.herokuapp.com/v1/funnel/cache"

    querystring = {"agent_id":username,"flight_string":"SFO*2018-10-15T07:20*CQ*123*JFK*2018-10-15T08:45","adults":adults}
    if children != 0:
        querystring["children"] = children
    if infants != 0:
        querystring["infants"] = infants

    gordian_response = requests.request("GET", url, params=querystring)
    session_id = gordian_response.json().get('session_id')
    print(session_id)
    request.session['gordian_session_id'] = session_id
    return render(request, 'details.html', {"name": username, "adults": adults, "infants": infants, "children": children, 'session_id': session_id})
    
def detail(request):
    if request.method == "POST":
        # Get the posted form
        DetailForm = DetailsForm(request.POST)

        if DetailForm.is_valid():
            username = DetailForm.cleaned_data['name']
            adults = DetailForm.cleaned_data['adults']
            infants = DetailForm.cleaned_data['infants']
            children = DetailForm.cleaned_data['children']
            
            #Storing data in session cache for later. 
            #In a real system, this would ideally be eventually put into a database (safer).
            
            request.session['temp_data'] = DetailForm.cleaned_data
            return redirect('seat_pick')
    else:
        DetailForm = DetailsForm()
        username = None
        adults = 0
        infants = 0
        children = 0
    return render(request, 'index.html', {"form": DetailForm, "user": username, "adults": adults, "infants": infants, "children": children})
