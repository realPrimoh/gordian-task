from django import forms

class DetailsForm(forms.Form):
    name = forms.CharField(max_length = 100)
    adults = forms.IntegerField(max_value = 3)
    children = forms.IntegerField(max_value = 3, initial = 0)
    infants = forms.IntegerField(max_value = 3, initial = 0)
    